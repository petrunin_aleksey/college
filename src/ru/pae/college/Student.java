package ru.pae.college;

public class Student extends Person {
    private int ydull;
    private String specialty;

    public Student(int ydull, String specialty, String surname, Gender gender) {
        super(surname, gender);
        this.ydull = ydull;
        this.specialty = specialty;
    }

    public int getYdull() {
        return ydull;
    }

    public String getSpecialty() {
        return specialty;
    }

    @Override
    public String toString() {
        return "Студент - " +
                "Год поступления - " + ydull +
                ", Специальность - " + specialty + '\'' +
                ')';
    }
}