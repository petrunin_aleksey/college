package ru.pae.college;

public class Teacher extends Person{
    private String discipline;
    private boolean curator;
    public Teacher(String discipline, boolean curator, String surname, Gender gender) {
        super(surname, gender);
        this.discipline = discipline;
        this.curator =curator;
    }

    public String getDiscipline() {
        return discipline;
    }

    public boolean isCurator() {
        return curator;
    }

    @Override
    public String toString() {
        return "Учитель" +
                "Дисциплина - " + discipline + '\'' +
                ", Куратор - " + curator +
                ')';
    }
}